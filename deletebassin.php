<?php
require 'bdd/bddconfig.php';
$paramOk = false;

if (isset($_POST["idbassin"])) {
    $idbassin = intval(htmlspecialchars($_POST["idbassin"]));
    $paramOk = true;
}

if ($paramOk == true) {
    try {
        $objBdd = new PDO("mysql:host=$bddserver;
        dbname=$bddname;
        charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //supprime les temperatures de ce bassin
        $RSlogins = $objBdd->prepare("DELETE FROM temperature WHERE idBassin =:id");
        $RSlogins->bindParam(':id', $idbassin, PDO::PARAM_INT);
        $RSlogins->execute();

        //supprime le bassin de la table bassin
        $RSlogins = $objBdd->prepare("DELETE FROM bassin WHERE idBassin =:id");
        $RSlogins->bindParam(':id', $idbassin, PDO::PARAM_INT);
        $RSlogins->execute();


        /*redirige vers une page différente du dossier courant */
        $serveur = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
        $page = 'bassins.php';
        //header("Location: http://$serveur$chemin/$page");
    } catch (Exception $prme) {
        die('Erreur : ' . $prme->getMessage());
    }
} else {
    die('Les paramètres reçus ne sont pas valides');
}
