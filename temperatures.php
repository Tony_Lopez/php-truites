<?php $titre = "Les températures";

$idBassin = 0;
$nomBassin = "Bassin inconnu";

$idok = isset($_GET['idBassin']);
$nomok = isset($_GET['nomBassin']);
if (($idok = true) && ($nomok = true)) {
    $idBassin = htmlspecialchars($_GET['idBassin']);
    $nomBassin = htmlspecialchars($_GET['nomBassin']);
}

?>

<?php
//requete SQL
require "bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;
                dbname=$bddname;
                charset=utf8", $bddlogin, $bddpass);

$listeTemperatures = $objBdd->prepare("SELECT * FROM temperature
                         WHERE idBassin = :id
                           ORDER BY date DESC");
$listeTemperatures->bindParam(':id', $idBassin, PDO::PARAM_INT);
$listeTemperatures->execute();

$listeTemperatures = $objBdd->query("SELECT * FROM temperature
                      WHERE idBassin = :id
                       ORDER BY date DESC");

?>

<?php ob_start(); ?>

<article>
    <h1>Les temperatures : <?= $nomBassin; ?></h1>
    <?= "idBassin = " . $_GET['idBassin']; ?>

    <table>
        <thead>
            <tr>
                <th>Date</th>
                <th>Température (°C)</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listeTemperatures as $temp) { ?>
                <tr>
                    <td><?php echo $temp['date']; ?></td>
                    <td><?php echo $temp['temp']; ?></td>
                </tr>
            <?php }
            $listeTemperatures->closeCursor();
            ?>
        </tbody>
    </table>
</article>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>