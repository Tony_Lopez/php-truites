<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title><?= $titre; ?></title>
    <link rel="stylesheet" href="css/styles.css" />
</head>

<body>
    <div id="conteneur">
        <header>
            <?php
            //Accès seulement si authentifié
            if (isset($_SESSION['logged_in']['login']) == TRUE) {
            ?>
                <a href="logout.php">déconnexion</a>
            <?php
                // Redirige vers la page d'accueil (ou login.php) si pas authentifié
                echo $_SESSION['logged_in']['prenom'] . ' ' . $_SESSION['logged_in']['nom'];
            } else {
            ?>
                <a href="login.php">connexion</a>
            <?php
            }
            ?>
    </div>
    <h1>La pisciculture PHP</h1>
    </header>

    <nav>
        <ul>
            <li><a href="index.php">Accueil</a></li>
            <li><a href="bassins.php">Les bassins</a></li>
            <li><a href="arcenciel.php">La truite arc-en-ciel</a></li>
            <?php if (isset($_SESSION['logged_in']['login']) == TRUE) { ?>
                <li><a href="ajouterbassin.php">Ajouter un bassin</a></li>
                <li><a href="supprbassin.php">Supprimer un bassin</a></li>
            <?php } ?>
        </ul>
    </nav>
    <section>

        <?php echo $contenu; ?>

    </section>

    <footer>
        <p>Copyright TruitesPHP - Tous droits réservés -
            <a href="#">Contact</a>
        </p>
    </footer>
    </div>
</body>

</html>