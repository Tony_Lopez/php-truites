<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>La pisciculture PHP</title>
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <div id="conteneur">
            <header>
                <h1>La pisciculture PHP</h1>
            </header>

            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="bassins.php">Les bassins</a></li>
                    <li><a href="arcenciel.php">La truite arc-en-ciel</a></li>
                </ul>
            </nav>
            <section>
                <article> 
                    <h1>La truite arc-en-ciel</h1>
                    <figure>
                        <img src="images/truite-arc-en-ciel.jpg" alt="Bassins du Hem" />
                        <figcaption>Oncorhynchus mykiss</figcaption>
                    </figure>        
                    <p>La truite arc-en-ciel (Oncorhynchus mykiss) est un salmonidé 
                        originaire du sous-continent nord-américain où il est commun, 
                        mais se trouvant également en Europe et en Amérique du Sud, 
                        où il a été introduit.</p>
                    <ul>
                        <li>Longueur maximale observée : 120 cm pour le mâle</li>
                        <li>Poids maximum observé : 25 kg</li>
                        <li>Longévité maximale observée : 11 ans</li>
                    </ul>
                    <p>La truite peut se reproduire à partir de deux ou trois ans. 
                        Elle se reproduit de novembre à janvier dans 
                        une eau entre 5 et 12 °C.</p>
                </article>
            </section>

            <footer>
                <p>Copyright TruitesPHP - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>